import React, { useEffect, useReducer } from 'react'
import { useRoutes } from 'hookrouter'
import 'sanitize.css'
import './App.scss'
import { Auth } from './components/authpage'
import ErrorPage from './components/errorpage'
import FeedPage from './components/feedpage'
import { Modal } from './components/modal'
import Profile from './components/profilepage'
import { initState, reducer, Store } from './_store'

// see './_config.js' for back-end path

function App() {
  const initObject = JSON.parse(sessionStorage.getItem('globalState')) || initState
  const [globalState, dispatch] = useReducer(reducer, initObject)

  useEffect(() => {
    sessionStorage.setItem('globalState', JSON.stringify(globalState))
    // console.log(initState)   // modal: {display: false, render: ƒ}
    // console.log(globalState) // modal: {} ???
  }, [globalState])

  const routes = {
    '/': () => globalState.token ? <FeedPage /> : <Auth />,
    '/p/:postId': ({ postId }) => <FeedPage postId={postId} />,

    '/me': () => globalState.token ? <Profile view={'timeline'} username={globalState.profile.username} /> : <Auth />,
    '/profile': () => globalState.token ? <Profile view={'timeline'} username={globalState.profile.username} /> : <Auth />,
    '/self': () => globalState.token ? <Profile view={'timeline'} username={globalState.profile.username} /> : <Auth />,
    '/:username': ({ username }) => <Profile view={'timeline'} username={username} />, 

    '/me/about': () => globalState.token ? <Profile view={'about'} username={globalState.profile.username} /> : <Auth />,
    '/profile/about': () => globalState.token ? <Profile view={'about'} username={globalState.profile.username} /> : <Auth />,
    '/self/about': () => globalState.token ? <Profile view={'about'} username={globalState.profile.username} /> : <Auth />,
    '/:username/about': ({ username }) => <Profile view={'about'} username={username} />, 

    '/me/friends': () => globalState.token ? <Profile view={'friends'} username={globalState.profile.username} /> : <Auth />,
    '/profile/friends': () => globalState.token ? <Profile view={'friends'} username={globalState.profile.username} /> : <Auth />,
    '/self/friends': () => globalState.token ? <Profile view={'friends'} username={globalState.profile.username} /> : <Auth />,
    '/:username/friends': ({ username }) => <Profile view={'friends'} username={username} />
  }
  const routeResult = useRoutes(routes) || <ErrorPage />
  const { authstyle, background, render } = globalState.modal

  return (
    <Store.Provider value={{ globalState, dispatch }}>
      {routeResult}
      {render ? <Modal authstyle={authstyle} background={background} render={render} /> : <></>}
    </Store.Provider>
  )
}

export default App
