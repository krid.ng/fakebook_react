// see back-end repository
// https://gitlab.com/sambrazil/codecamp3_fakebook_api

//const host = process.env.NODE_ENV === 'development' ? 'http://localhost:3001' : 'http://35.186.150.86:3001'
// const host = 'http://localhost:3001'
const host = 'http://35.185.183.142:3001'
const checkLikeComment = host + '/api/post/check_like_comment'
const checkLikePost = host + '/api/post/check_like_post'
const checkFriendStatus = host + '/api/post/check_friend_status'
const clearNotifyNumber = host + '/api/post/update_notification_number'
const createFeedComment = host + '/api/post/create_feed_comment'
const createFeedPost = host + '/api/post/create_feed_post'
const editFriendListPrivacy = host + '/api/post/post_friend_list_setting'
const editPost = host + '/api/post/edit_post'
const editSelfProfile = host + '/api/post/edit_profile'
const editSelfProfilePrivacy = host + '/api/post/post_profile_setting'
const editSinglePostPrivacy = host + '/api/post/edit_post_privacy'
const loginPath = host + '/api/login'
const logoutPath = host + '/api/logout'
const registerPath = host + '/api/register'
const removeComment = host + '/api/post/remove_comment'
const removefriend = host + '/api/post/remove_friend'
const removePost = host + '/api/post/remove_post'
const requestfriend = host + '/api/post/friend_request'
const responsefriend = host + '/api/post/friend_response'
const search = host + '/api/post/search'
const showAlbumPostPictures = host + '/api/post/show_post_picture'
const showFeedComment = host + '/api/post/show_user_feed_comment'
const showFriendList = host + '/api/post/friend_list'
const showFriendListPrivacy = host + '/api/post/show_friend_list_setting'
const showMixedFeedPost = host + '/api/post/show_all_feed_post'
const showNotifyList = host + '/api/post/show_notification'
const showPendingList = host + '/api/post/show_friend_response'
const showRecommendedList = host + '/api/post/friend_recommend'
const showRequestingList = host + '/api/post/show_friend_request'
const showSelfFeedPost = host + '/api/post/show_user_feed_post'
const showSelfPostById = host + '/api/post/show_post_by_id'
const showSelfProfile = host + '/api/post/show_edit_profile'
const showSelfProfilePrivacy = host + '/api/post/show_profile_setting'
const toggleCommentLike = host + '/api/post/like_comment'
const togglePostLike = host + '/api/post/like_post'
const viewPrivateFriendList = host + '/api/post/view_private_friend_list'
const viewPrivatePostById = host + '/api/post/view_private_post_by_id'
const viewPrivateProfile = host + '/api/post/view_private_profile'
const viewPublicFriendList = host + '/api/get/view_friend_list'
const viewPublicProfile = host + '/api/get/view_profile'
const viewUserFeedPost = host + '/api/post/view_private_post'
const viewUserPublicFeedPost = host + '/api/get/view_post'

export {
  checkLikeComment, 
  checkLikePost, 
  checkFriendStatus, 
  clearNotifyNumber, 
  createFeedComment, 
  createFeedPost, 
  editFriendListPrivacy, 
  editPost, 
  editSelfProfile, 
  editSelfProfilePrivacy, 
  editSinglePostPrivacy, 
  host, 
  loginPath, 
  logoutPath, 
  registerPath, 
  removeComment, 
  removefriend, 
  removePost, 
  requestfriend, 
  responsefriend, 
  search, 
  showAlbumPostPictures, 
  showFeedComment, 
  showFriendList, 
  showFriendListPrivacy, 
  showMixedFeedPost, 
  showNotifyList, 
  showPendingList, 
  showRecommendedList, 
  showRequestingList, 
  showSelfFeedPost, 
  showSelfPostById, 
  showSelfProfile, 
  showSelfProfilePrivacy, 
  toggleCommentLike, 
  togglePostLike, 
  viewPrivateFriendList, 
  viewPrivatePostById, 
  viewPrivateProfile, 
  viewPublicFriendList, 
  viewPublicProfile, 
  viewUserFeedPost, 
  viewUserPublicFeedPost
}