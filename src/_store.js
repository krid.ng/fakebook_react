import { createContext } from 'react'

const initState = {
  friendlist: [],
  modal: {
    authstyle: false, 
    background: '', 
    render: false
  }, 
  profile: {
    avatar: '', 
    bio: '', 
    birthday: '', 
    come_from: '', // profile
    cover: '', // profile
    education: '', // profile
    email: '', 
    firstname: '', 
    gender: '', 
    hobbies: '', // profile
    lastname: '', 
    lives: '', // profile
    relationship_status: '', // profile
    username: '', 
    work_place: '' // profile
  }, 
  token: ''
}

function reducer(state, action) {
  const friendlist = action.friendlist || []
  const profile = action.profile || {}
  const { bio, birthday, email, firstname, gender, id, lastname, profile_picture_url, username } = profile
  switch (action.type) {
    case 'EDITPROFILE':
      const { name, value } = action
      return {
        ...state, 
        "profile": {
          ...state.profile, 
          [name]: value
        }
      }
    case 'LOGIN':
      return {
        ...state, 
        "profile": {
          ...state.profile, 
          "avatar": profile_picture_url, 
          bio, 
          birthday, 
          email, 
          firstname, 
          gender, 
          lastname, 
          username
        }, 
        "token": id
      }
    case 'LOGOUT':
      return initState
    case 'SETFRIENDLIST':
      return {
        ...state, 
        friendlist
      }
    case 'SETMODAL':
      return {
        ...state, 
        "modal": {
          "authstyle": action.authstyle, 
          "background": action.background, 
          "render": action.render
        }
      }
    default:
      return state
  }
}

const Store = createContext()

export { initState, reducer, Store }