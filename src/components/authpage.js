import React, { useContext, useEffect, useState } from 'react'
import { navigate } from 'hookrouter'
import { loginPath, registerPath } from '../_config'
import { Store } from '../_store'
import { fetching } from './sharedapi'
import { CloseButton } from './svg'
import brand from '../images/brand.png'
import worldwidenetwork from '../images/worldwidenetwork.png'

let passwordPattern = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)

const handleLogIn = async (e, data, dispatch) => {
  if (e) {
    e.preventDefault()
  }
  const [email, password] = data
  await fetching(loginPath, {
    email, 
    password
  }, {
    "callback": (response) => {
      if (response.id) {
        dispatch({ "type": "LOGIN", "profile": { ...response, email } })
        window.location.reload()
      }
    }, 
    //"log": "AuthLogIn.handleLogIn"
  })
}

function Auth() {
  const { dispatch } = useContext(Store)
  const useInfo = useState({
    loginemail: '', 
    loginpassword: '', 
    rfirstname: '', 
    rlastname: '', 
    remail: '', 
    rpassword: '', 
    rconfirmpassword: '', 
    rbirthday: '', 
    rgender: ''
  })
  const [info, setInfo] = useInfo

  const handleChange = (e) => {
    if (e.target.name) {
      setInfo({ ...info, [e.target.name]: e.target.value })
    } else {
      setInfo({ ...info, [e.target.id]: e.target.value })
    }
  }

  const toggleModal = () => {
    dispatch({ "type": "SETMODAL", "authstyle": true, "render": () => (
      <AuthSignUp manageInfo={{ useInfo, handleChange }} />
    ) })
  }

  return (
    <>
      <AuthLogIn manageInfo={{ info, handleChange }} toggleModal={toggleModal} />
      <div className="auth-footer">
        Fakebook &copy; 2019
      </div>
    </>
  )
}

function AuthLogIn(props) {
  const { dispatch } = useContext(Store)
  const { manageInfo, toggleModal } = props
  const { info, handleChange } = manageInfo
  const { loginemail, loginpassword } = info

  return (
    <div className="auth-loginpage">
      <div className="container">
        <div className="left">
          <img className="brand" src={brand} alt="fakebook" />
          <p>Fakebook helps you to connect and share with your people</p>
          <img className="decorate" src={worldwidenetwork} alt="decorate" />
        </div>
        <div className="right">
          <div className="loginbox">
            <form onSubmit={(e) => handleLogIn(e, [loginemail, loginpassword], dispatch)}>
              <input id="loginemail" type="email" placeholder="E-mail" required
                value={loginemail} onChange={handleChange}
              />
              <input id="loginpassword" type="password" placeholder="Password" required
                value={loginpassword} onChange={handleChange}
              />
              <button id="loginsubmit" type="submit">Login</button>
              {/*<A href="#">Forgot Password</A>*/}
            </form>
            <hr />
            <button id="signuptoggle" onClick={toggleModal}>
              Create New Account
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

function AuthSignUp(props) {
  const { dispatch } = useContext(Store)
  const [info, setInfo] = useState({
    "rfirstname": '', 
    "rlastname": '', 
    "remail": '', 
    "rpassword": '', 
    "rconfirmpassword": '', 
    "rbirthday": '', 
    "rgender": ''
  })
  const {
    rfirstname, 
    rlastname, 
    remail, 
    rpassword, 
    rconfirmpassword, 
    rbirthday, 
    rgender
  } = info
  const useWarning = useState({
    "forEmail": 'none', 
    "forPassword": 'inline', 
    "forConfirmpassword": 'none'
  })
  const [warning, setWarning] = useWarning
  const { forEmail, forPassword, forConfirmpassword } = warning

  const handleChange = (e) => {
    // cannot use setInfo() in Auth, although e.target is valid
    if (e.target.name) {
      setInfo({ ...info, [e.target.name]: e.target.value })
    } else {
      setInfo({ ...info, [e.target.id]: e.target.value })
    }
  }

  const handleSignUp = async (e) => {
    e.preventDefault()
    if (rpassword !== rconfirmpassword || !passwordPattern.test(rpassword)) {
      return
    }
    await fetching(registerPath, {
      "firstname": rfirstname,
      "lastname": rlastname, 
      "password": rpassword, 
      "email": remail, 
      "birthday": rbirthday, 
      "gender": rgender
    }, {
      "callback": (response) => {
        if (response.status.substring(0, 5) === 'email') {
          setWarning({ ...warning, "forEmail": 'inline' })
          return
        }
        dispatch({ "type": "SETMODAL", "render": false })
        handleLogIn(null, [remail, rpassword], dispatch)
      }, 
      //"log": "AuthSignUp.handleSignUp"
    })
  }

  useEffect(() => {
    let passwordWarn
    let confirmpasswordWarn
    if (passwordPattern.test(rpassword)) {
      passwordWarn = 'none'
    } else {
      passwordWarn = 'inline'
    }
    if (rpassword === rconfirmpassword) {
      confirmpasswordWarn = 'none'
    } else {
      confirmpasswordWarn = 'inline'
    }
    setWarning({ ...warning, "forPassword": passwordWarn, "forConfirmpassword": confirmpasswordWarn })
  }, [rpassword, rconfirmpassword])

  useEffect(() => {
    setWarning({ ...warning, "forEmail": 'none' })
  }, [remail])

  return (
    <>
      <CloseButton onClick={() => dispatch({ "type": "SETMODAL", "render": false })} />
      <div className="title">
          <h1>Sign Up</h1>
          <span className="right">Easy and Fast</span>
      </div>
      <hr />
      <div>
        <form onSubmit={handleSignUp}>
            <input id="rfirstname" placeholder="FirstName" required
              value={rfirstname} onChange={handleChange}
            />
            <input id="rlastname" placeholder="LastName" required
              value={rlastname} onChange={handleChange}
            />
            <div className="warningframe">
              <input id="remail" type="email" placeholder="Email Address" required
                value={remail} onChange={handleChange}
              />
              <span className="warning" style={{ "display": forEmail }}>
                This e-mail has already registered.
              </span>
            </div>
            <div className="warningframe">
              <input id="rpassword" type="password" placeholder="Password" required
                value={rpassword} onChange={handleChange}
              />
              <span className="warning" style={{ "display": forPassword }}>
                Min 8 chars, including Uppercase, Lowercase & Number (No Specials)
              </span>
            </div>
            <div className="warningframe">
              <input id="rconfirmpassword" type="password" placeholder="Confirm Password Again" required
                value={rconfirmpassword} onChange={handleChange}
              />
              <span className="warning" style={{ "display": forConfirmpassword }}>
                Password & ConfirmPassword must be the same.
              </span>
            </div>
            <label htmlFor="rbirthday">Birthday</label>
            <input id="rbirthday" type="date" required
              value={rbirthday} onChange={handleChange}
            />
            <label htmlFor="rgender">Gender</label>
            <div className="radiogroup">
              <div>
                <label htmlFor="female">Female</label>
                <input type="radio" name="rgender" id="female" required
                  value="female" onChange={handleChange} checked={rgender === "female"}
                />
              </div>
              <div>
                <label htmlFor="male">Male</label>
                <input type="radio" name="rgender" id="male" required
                  value="male" onChange={handleChange} checked={rgender === "male"}
                />
              </div>
              <div>
                <label htmlFor="other">Other</label>
                <input type="radio" name="rgender" id="other" required
                  value="other" onChange={handleChange} checked={rgender === "other"}
                />
              </div>
            </div>
            <p>
              When you sign up, you accept our Terms of Service, Privacy Policy, and Cookie Policy.
            </p>
            <button id="rsubmit" type="submit">Sign Up</button>
        </form>
      </div>
    </>
  )
}

export { Auth, handleLogIn }