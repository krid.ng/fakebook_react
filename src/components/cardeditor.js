import React, { useContext, useEffect, useRef, useState } from 'react'
import { createFeedPost, editPost, host } from '../_config'
import { Store } from '../_store'
import PrivacySelector from './privacyselector'
import { fetching } from './sharedapi'
import { CloseButton, Friends, Globe, Private } from './svg'
import defaultAvatar from '../images/default_avatar.jpg'
import picture from '../images/picture.jpg'

const addPicture = (imgRef, useContent) => {
  if (imgRef.current.files.length) {
    const [content, setContent] = useContent
    const reader = new FileReader()
    reader.readAsDataURL(imgRef.current.files[0])
    reader.onloadend = () => {
      setContent({ ...content, "img": [[imgRef.current.files[0], reader.result]] })
    }
  }
}

const handleChange = (e, useContent) => {
  const [content, setContent] = useContent
  setContent({ ...content, "caption": e.target.innerText })
}

const handleUpload = async (e, data) => {
  const { captionRef, editData, globalState, imgRef, privacy, showFeed, useFeed, useContent } = data
  const { post_id } = editData || { 'post_id': '' }
  const [content, ] = useContent
  const { caption, img } = content
  if (!caption.trim() && !img.length) return
  let newText = captionRef.current.innerText.trim()
  if (newText.length > 255) {
    newText = newText.substring(0, 255)
  }
  let picture = img.length ? img[0][0] : ''
  if (editData) {
    await fetching(editPost, {
      post_id, 
      "username": globalState.profile.username, 
      "caption": newText
    }, {
      "callback": (response) => {
        window.location.reload()
      }, 
      //"log": `CardEditor.handleUpload(edit ${post_id})`
    })
  } else {
    await fetching(createFeedPost, {
      "id": globalState.token, 
      caption, 
      picture, 
      privacy
    }, {
      "callback": (response) => {
        /* TODO: found error on the 1st post creation
        showFeed(useFeed, globalState.token, username, { "refreshFeed": true, abortcontroller })
        setContent({ "caption": '', "img": [] })
        captionRef.current.innerText = ''
        imgRef.current.FileList = null*/
        window.location.reload()
      }, 
      //"log": "CardEditor.handleUpload(create)"
    })
  }
}

const removePicture = (removeIndex, useContent) => {
  const [content, setContent] = useContent
  const { img } = content
  const newImg = img.filter((each, index) => index !== removeIndex)
  setContent({ ...content, "img": newImg })
}

function CardEditor(props) {
  const { globalState } = useContext(Store)
  const { callback, editData } = props
  const useContent = useState({
    caption: '', 
    img: []  // [[imgSrc, imgBinary], ...]
  })
  const [content, ] = useContent
  const { img } = content

  const displayAvatar = (globalState.profile.avatar && globalState.profile.avatar !== ' ') ? `${host}${globalState.profile.avatar}` : defaultAvatar
  const captionRef = useRef()
  const imgRef = useRef()
  const { showFeed, useFeed } = callback
  const caption = editData ? editData.caption : ''
  const picture_url = editData ? editData.picture_url : ''
  const post_id = editData ? editData.post_id : ''
  const [privacy, setPrivacy] = useState('public')

  useEffect(() => {
    if (caption !== '') {
      captionRef.current.innerText = caption
    }
    /*if (picture_url !== '') {
      const request = new XMLHttpRequest()
      request.open('GET', `${host}${picture_url}`, true)
      request.responseType = 'blob'
      request.onload = function() {
        const reader = new FileReader()
        reader.readAsDataURL(request.response)
        reader.onload =  function(e) {
          //console.log('DataURL:', e.target.result)
          setContent({ ...content, "img": [picture_url, request.response] })
        }
      }
      request.send()
    }*/
  }, [post_id])

  return (
    <div className="card-editor" style={editData ? { "margin": "0 auto" } : {}}>
      <div className="title">
        {editData ? 'Edit Post' : 'Create New Post'}
      </div>
      <div className="body">
        <img src={displayAvatar} alt='avatar' />
        <div ref={captionRef} className="caption" name="caption" contentEditable="true"
          onInput={(e) => handleChange(e, useContent)}
        />
        <div className="picturepreviewframe">
          {img.length ? 
            img.map((each, index) => {
              const [imgSrc, imgBinary] = each
              return (
                <div className="picture" key={index}>
                  <img name="picture" src={imgBinary} alt={`${index}.${imgSrc}`} />
                  <CloseButton onClick={() => removePicture(index, useContent)} />
                </div>
              )
            }) : <></>
          }
        </div>
        {editData ? 
          <></> :
          <div className="feature">
            <input ref={imgRef} type="file" accept="image/*" onChange={() => addPicture(imgRef, useContent)} />
            <button onClick={() => imgRef.current.click()} className="featurebutton">
              <img className="picicon" src={picture} alt="picicon" /> Picture
            </button>
            <PrivacySelector data={{ privacy, setPrivacy, "useFor": "cardeditor" }} />
          </div>
        }
      </div>
      <div className="foot">
        <button onClick={(e) => handleUpload(e, { captionRef, editData, globalState, imgRef, privacy, showFeed, useFeed, useContent })}>Post</button>
      </div>
      {picture_url ?
        <img className="postedpicture" src={`${host}${picture_url}`} alt='picOnServer' />
        : <></>
      }
    </div>
  )
}

export default CardEditor