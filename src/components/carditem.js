import React, { useContext, useEffect, useState } from 'react'
import { A } from 'hookrouter'
import { checkLikePost, host, removePost, togglePostLike } from '../_config'
import { Store } from '../_store'
import CardEditor from './cardeditor'
import CommentItem from './commentitem'
import CommentEditor from './commenteditor'
import { BasicModal } from './modal'
import PrivacySelector from './privacyselector'
import { fetching, getMonthString } from './sharedapi'
import { CloseButton, Friends, Globe, Private, Thumbsup } from './svg'
import comment from '../images/comment-alt-regular.svg'
import defaultAvatar from '../images/default_avatar.jpg'

const calculatePostTime = (now, posttime) => {
  let postTime = posttime
  if (!Number.isInteger(posttime)) {
    if (Number.isInteger(parseInt(posttime))) {
      postTime = parseInt(posttime)
    } else {
      return posttime
    }
  }
  let diffTime = now - postTime
  if (diffTime < 60000) {
    return 'just now'
  }
  if (diffTime < 120000) {
    return '1 minute ago'
  }
  if (diffTime < 3600000) {
    return `${Math.floor(diffTime/60000)} minutes ago`
  }
  if (diffTime < 7200000) {
    return `1 hour ago`
  }
  if (diffTime < 86400000) {
    return `${Math.floor(diffTime/3600000)} hours ago`
  }
  const showTwoDigits = (int) => {
    if (int < 10) {
      return `0${int}`
    }
    return int
  }
  let postDate = new Date(postTime)
  if (diffTime < 172800000) {
    let yesterday = new Date(now - 86400000)
    if (yesterday.getDate() === postDate.getDate())
      return `yesterday ${showTwoDigits(postDate.getHours())}:${showTwoDigits(postDate.getMinutes())}`
  }
  let year = ''
  let nowDate = new Date(now)
  if (nowDate.getFullYear() !== postDate.getFullYear()) {
    year = ` ${postDate.getFullYear()}`
  }
  return `${postDate.getDate()} ${getMonthString(postDate.getMonth())}${year}, ${showTwoDigits(postDate.getHours())}:${showTwoDigits(postDate.getMinutes())}`
}

function CardItem(props) {
  const { data, now, showComments, showFeed, useFeed } = props
  const { caption, comments, commentsContent, created_at, firstname, lastname, likes, picture_url, post_id, privacy, profile_picture_url, username } = data
  const { globalState, dispatch } = useContext(Store)
  const useController = useState({
    "commenteditor": "none", 
    "commentsnumber": comments,
    "liked": false, 
    "likesnumber": parseInt(likes), 
    "menufocus": "none"
  })
  const [controller, setController] = useController
  const { commenteditor, commentsnumber, liked, likesnumber, menufocus } = controller
  const displayAvatar = (profile_picture_url && profile_picture_url !== ' ') ? `${host}${profile_picture_url}` : defaultAvatar

  const deletePost = async () => {
    await fetching(removePost, {
      post_id, 
      username, 
      picture_url
    }, {
      "callback": (response) => {
        window.location.reload()
      }, 
      //"log": "CardItem.handleDelete"
    })
  }

  const handleBlur = () => {
    setTimeout(() => {
      setController({ ...controller, "menufocus": "none" })
    }, 200)
  }

  const handleClickComment = () => {
    setController({ ...controller, "commenteditor": "block" })
    showComments(useFeed, post_id, { useController })
  }

  const handleDelete = () => {
    dispatch({ 
      "type": "SETMODAL", 
      "background": 'rgba(0, 0, 0, .7)', 
      "render": () => (
        <BasicModal onClick={deletePost} title='Delete Post' body='Are you sure to delete this post?' />
      ) 
    })
  }

  const handleEditor = () => {
    dispatch({ 
      "type": "SETMODAL", 
      "background": 'rgba(0, 0, 0, .7)', 
      "render": () => (
        <>
          <CardEditor
            callback={{ showFeed, useFeed }} 
            editData={data}
          />
          <CloseButton color='#ccd0d5' size='12' onClick={() => dispatch({ "type": "SETMODAL", "render": false })} />
        </>
      )
    })
  }

  const handleFocus = () => {
    setController({ ...controller, "menufocus": "block" })
  }

  const togglingPostLike = async () => {
    let newStatus = liked ? 'unlike' : 'like'
    await fetching(togglePostLike, {
      "id": globalState.token, 
      post_id, 
      "status": newStatus
    }, {
      "callback": (response) => {
        if (liked) {
          setController({ ...controller, "liked": !liked, "likesnumber": likesnumber - 1})
        } else {
          setController({ ...controller, "liked": !liked, "likesnumber": likesnumber + 1})
        }
      }, 
      //"log": "CardItem.togglingPostLike"
    })
  }

  useEffect(() => {
    const abortcontroller = new AbortController();
    (async function() {
      await fetching(checkLikePost, {
        "id": globalState.token, 
        post_id
      }, {
        "callback": (response) => {
          if (response.status === 'like') {
            setController({ ...controller, "liked": true })
          } else {
            setController({ ...controller, "liked": false })
          }
        }, 
        //"log": `CardItem.checkLikePost ${post_id}`, 
        "signal": abortcontroller.signal
      })
    } ())
    return () => {
      abortcontroller.abort()
    }
  }, [])

  return (
    (caption || picture_url) ?
      <div className="card-item">
        <div className="title">
          <img src={displayAvatar} alt='author' />
          <A href={`/${username}`}>{`${firstname} ${lastname}`}</A>
          <span>
            {calculatePostTime(now, created_at)} &nbsp;
            {globalState.profile.username === username ?
              <PrivacySelector data={{ "useFor": "carditem", post_id, privacy }} /> :
              <></>
            }
            {privacy === 'public' && globalState.profile.username !== username ?
              <Globe /> :
              <></>
            }
            {privacy === 'friend' && globalState.profile.username !== username ? 
              <Friends /> : 
              <></>
            }
            {privacy === 'only me' && globalState.profile.username !== username ? 
              <Private /> : 
              <></>
            }
          </span>
        </div>
        {username === globalState.profile.username ?
          <div className="menu-toggle" onFocus={handleFocus} onBlur={handleBlur}>
            <button><span></span><span></span><span></span></button>
            <div className="menu" style={{display: menufocus}}>
              <button onClick={handleEditor}>Edit</button>
              <button onClick={handleDelete}>Delete</button>
            </div>
          </div>
          : <></>
        }
        <div className="body">
          <pre className="hyphenate">{caption}</pre>
          {picture_url ? <img src={`${host}${picture_url}`} alt='attached' /> : <></> }
        </div>
        <div className="interactivebar">
          {likesnumber ? <A href="#">{likesnumber} {likesnumber > 1 ? 'Likes' : 'Like'}</A> : <A href="#"></A>}
          {commentsnumber ? <A href="#" onClick={handleClickComment}>{commentsnumber} {commentsnumber > 1 ? 'Comments' : 'Comment'}</A> : <A href="#">&nbsp;</A>}
          {/*&nbsp;&nbsp;
          <A href="#">2 Shares</A>*/}
          <div>
            <button className="like" style={liked ? { "color": "#3784ff" } : {}} onClick={togglingPostLike}>
              <Thumbsup color={liked ? '#3784ff' : '#606770'} />{liked ? 'Liked' : 'Like'}
            </button>
            <button className="commenttoggle" onClick={handleClickComment}>
              <img className="icon" src={comment} alt='icon' />Comment
            </button>
            {/*<button className="share">Share</button>*/}
          </div>
        </div>
        <div className="commentzone" style={{ "display": commenteditor }}>
          { globalState.token ?
            <CommentEditor postId={post_id} callback={{ showComments, useFeed, useController }} />
            : <></>
          }
          { commentsContent.map(comment =>
            <CommentItem key={`${comment.post_id}/${comment.comment_id}`} data={comment} now={now} />
          )}
          { parseInt(comments) > commentsContent.length ?
            <A onClick={() => showComments(useFeed, post_id)} href="#">More Comments</A>
            : <A href=""></A>
          }
        </div>
      </div>
      : <></>
  )
}

export default CardItem