import React, { useContext, useRef, useState } from 'react'
import { createFeedComment, host } from '../_config'
import { Store } from '../_store'
import { fetching } from './sharedapi'
import { Camera, CloseButton } from './svg'
import defaultAvatar from '../images/default_avatar.jpg'

const addPictures = (imgRef, useContent) => {
  if (imgRef.current.files.length) {
    const [content, setContent] = useContent
    const reader = new FileReader()
    reader.readAsDataURL(imgRef.current.files[0])
    reader.onloadend = () => {
      setContent({ ...content, "img": [[imgRef.current.files[0], reader.result]] })
    }
  }
}

const handleUpload = async (e, data) => {
  const { commentRef, globalState, imgRef, postId, showComments, useContent, useController, useFeed } = data
  const [content, setContent] = useContent
  const [controller, setController] = useController
  const { comment, img } = content
  let newText = comment.trim()
  if (comment.substring(comment.length - 1, comment.length) === '\n') {
    newText = newText.substring(0, comment.length - 1)
  }
  if (newText.length > 255) {
    newText = newText.substring(0, 255)
  }
  
  if (e.keyCode === 13) {
    if (e.shiftKey) return
    if (newText.length || img.length) {
      let picture = img.length ? img[0][0] : ''
      const abortcontroller = new AbortController()
      await fetching(createFeedComment, {
        "id": globalState.token, 
        "post_id": postId, 
        "comment": newText, 
        picture
      }, {
        "callback": (response) => {
          showComments(useFeed, postId)
          setContent({ "comment": '', "img": [] })
          commentRef.current.innerText = ''
          imgRef.current.FileList = null
          setController({ ...controller, "commentsnumber": controller.commentsnumber + 1 })
        }, 
        //"log": "CommentEditor.handleUpload", 
        "signal": abortcontroller.signal
      })
    }
  }
}

const removePicture = (removeIndex, useContent) => {
  const [content, setContent] = useContent
  const { img } = content
  const newImg = img.filter((each, index) => index !== removeIndex)
  setContent({ ...content, "img": newImg })
}

function CommentEditor(props) {
  const { globalState } = useContext(Store)
  const displayAvatar = (globalState.profile.avatar && globalState.profile.avatar !== ' ') ? `${host}${globalState.profile.avatar}` : defaultAvatar
  const { showComments, useFeed, useController } = props.callback
  const useContent = useState({
    comment: '', 
    img: []  // [[imgSrc, imgBinary], ...]
  })
  const [content, setContent] = useContent
  const { img } = content
  const commentRef = useRef()
  const imgRef = useRef()

  const handleChange = (e) => {
    setContent({ ...content, "comment": e.target.innerText })
  }

  return (
    <div className="comment-editor">
      <img className="commentator" src={displayAvatar} alt='commentator' />
      <div className="commentframe">
        <pre name="comment" ref={commentRef} contentEditable="true" onInput={handleChange}
          onKeyUp={(e) => handleUpload(e, { commentRef, globalState, imgRef, "postId": props.postId, showComments, useContent, useController, useFeed })}
        />
        <input ref={imgRef} type="file" accept="image/*" onChange={() => addPictures(imgRef, useContent)} />
        {img.length ?
          <></> : 
          <Camera onClick={() => imgRef.current.click()} />
        }
        <div className="picturepreviewframe">
          {img.length ? 
            img.map((each, index) => {
              const [imgSrc, imgBinary] = each
              return (
                <div className="picture" key={index}>
                  <img name="picture" src={imgBinary} alt={`${index}.${imgSrc}`} />
                  <CloseButton onClick={() => removePicture(index, useContent)} />
                </div>
              )
            }) : <></>
          }
        </div>
      </div>
    </div>
  )
}

export default CommentEditor