import React, { useContext, useEffect, useState } from 'react'
import { A } from 'hookrouter'
import { checkLikeComment, host, removeComment, toggleCommentLike } from '../_config'
import { Store } from '../_store'
import { BasicModal } from './modal'
import { calculateCommentTime, fetching } from './sharedapi'
import defaultAvatar from '../images/default_avatar.jpg'
import like from '../images/like.png'

function CommentItem(props) {
  const { globalState, dispatch } = useContext(Store)
  const { data, now } = props
  const { picture_url, comment, comment_id, created_at, likes, username, firstname, lastname, profile_picture_url } = data
  const displayAvatar = (profile_picture_url && profile_picture_url !== ' ') ? `${host}${profile_picture_url}` : defaultAvatar
  const [controller, setController] = useState({
    "liked": false, 
    "likesnumber": parseInt(likes), 
    "menufocus": "none"
  })
  const { liked, likesnumber, menufocus } = controller

  const deleteComment = async () => {
    await fetching(removeComment, {
      comment_id, 
      username, 
      picture_url
    }, {
      "callback": (response) => {
        window.location.reload()
      }, 
      //"log": "CardItem.handleDelete"
    })
  }

  const handleBlur = () => {
    setTimeout(() => {
      setController({ ...controller, "menufocus": "none" })
    }, 300)
  }

  const handleDelete = () => {
    dispatch({ 
      "type": "SETMODAL", 
      "background": 'rgba(0, 0, 0, .7)', 
      "render": () => (
        <BasicModal onClick={deleteComment} title='Delete Comment' body='Are you sure to delete this comment?' />
      ) 
    })
  }

  const handleEditor = () => {

  }

  const handleFocus = () => {
    setController({ ...controller, "menufocus": "block" })
  }

  const togglingCommentLike = async () => {
    let newStatus = liked ? 'unlike' : 'like'
    await fetching(toggleCommentLike, {
      "id": globalState.token, 
      comment_id, 
      "status": newStatus
    }, {
      "callback": (response) => {
        if (liked) {
          setController({ ...controller, "liked": !liked, "likesnumber": likesnumber - 1})
        } else {
          setController({ ...controller, "liked": !liked, "likesnumber": likesnumber + 1})
        }
      }, 
      //"log": "CardItem.togglingPostLike"
    })
  }

  useEffect(() => {
    const abortcontroller = new AbortController();
    (async function() {
      await fetching(checkLikeComment, {
        "id": globalState.token, 
        comment_id
      }, {
        "callback": (response) => {
          if (response.status === 'like') {
            setController({ ...controller, "liked": true })
          } else {
            setController({ ...controller, "liked": false })
          }
        }, 
        //"log": `CardItem.checkLikeComment ${post_id} > ${comment_id}`, 
        "signal": abortcontroller.signal
      })
    } ())
    return () => {
      abortcontroller.abort()
    }
  }, [])

  return (
    (comment || picture_url) ?
      <div className="comment-item">
        <img className="commentator" src={displayAvatar} alt='commentator' />
        <div className="commentframe">
          <pre className="hyphenate">
            <span className='commentator'>{`${firstname} ${lastname}`}</span>
            &nbsp;{comment}
            {username === globalState.profile.username ?
              <div className="menu-toggle" onFocus={handleFocus} onBlur={handleBlur}>
                <button><span></span><span></span><span></span></button>
                <div className="menu" style={{display: menufocus}}>
                  {/*<button onClick={handleEditor}>Edit</button>*/}
                  <button onClick={handleDelete}>Delete</button>
                </div>
              </div>
              : <></>
            }
            {likesnumber && !picture_url ? 
              <span className="likesnumber">
                <img className="like" src={like} alt="like" /> {likesnumber}
              </span> 
              : <></>
            }
          </pre>
          <div className="pictureframe">
            {picture_url ? 
              <img src={`${host}${picture_url}`} className="attached" alt={picture_url} /> 
              : <></>
            }
            {likesnumber && picture_url ? 
              <span className="likesnumber">
                <img className="like" src={like} alt="like" /> {likesnumber}
              </span> 
              : <></>
            }
          </div>
          <A className="interactive" style={liked ? { "color": "#3784ff" } : {}} onClick={togglingCommentLike} href="#">
            {liked ? 'Liked' : 'Like'}
          </A>
          ·&nbsp;{calculateCommentTime(now, created_at)}
        </div>
      </div>
    : <></>
  )
}

export default CommentItem