import React from 'react'
import { A } from 'hookrouter'

function ErrorPage(props) {
  return (
    <div className="error-page">
      <p>{props.errorMessage ? props.errorMessage : 'Sorry, Something might be wrong'}</p>
      <A href="/">Go to Home</A>
    </div>
  )
}

export default ErrorPage