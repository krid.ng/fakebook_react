import React, { useContext, useEffect, useState } from 'react'
import { showFeedComment, showMixedFeedPost, showSelfFeedPost, showSelfPostById, viewPrivatePostById, viewUserFeedPost, viewUserPublicFeedPost } from '../_config'
import { Store } from '../_store'
import CardEditor from './cardeditor'
import CardItem from './carditem'
import { fetching } from './sharedapi'
import openroad from '../images/openroad.svg'

const showComments = async (useFeed, postId, options) => {
  const useCommentController = options ? options.useController || [null, null] : [null, null]
  const abortcontroller = options ? options.abortcontroller : null
  const signal = abortcontroller ? abortcontroller.signal : null
  const [feed, setFeed] = useFeed
  const postIdIndex = feed.findIndex(post => post.post_id === postId)
  if (postIdIndex === -1) return
  const post = feed[postIdIndex]
  let originComments = post.commentsContent
  const startAt = options ? options.startAt || originComments.length + 1 : originComments.length + 1
  const per_page = 5
  await fetching(showFeedComment, {
    "post_id": postId, 
    "start_row": startAt, 
    per_page
  }, {
    "callback": (response) => {
      //console.log(`post_id ${postId}'s comments: `)
      //console.log(response.data)
      if (!response.data) return
      if (response.data.length === 0) return
      /*let newComments = [...response.data]
      console.log('newComments')
      console.log(newComments)     // amazing array 0 length bug on the 2nd call (maybe const too!!)
      // removeDuplicates()
      let mergedComments = []
      if (originComments.length) {
        let originEach = {}
        let newEach = {}
        while (originComments.length || newComments.length || originEach.length || newEach.length ) {
          if (originComments.length && !originEach.length) originEach = originComments.shift()
          if (newComments.length && !originEach.length) newEach = newComments.shift()
          if (!originEach.length) {
            mergedComments.push(newEach)
            newEach = {}
            continue
          }
          if (!newEach.length) {
            mergedComments.push(originEach)
            originEach = {}
            continue
          }
          if (originEach['comment_id'] === newEach['comment_id']) {
            mergedComments.push({...newEach})
            originEach = {}
            newEach = {}
            continue
          }
          if (originEach['comment_id'] < newEach['comment_id']) {
            mergedComments.push(originEach)
            originEach = {}
          } else {
            mergedComments.push(newEach)
            newEach = {}
          }
          console.log(mergedComments)
        }
      } else {
        mergedComments = [...newComments]
      }*/
      const newComments = response.data.filter(newEach =>
        originComments.findIndex(originEach => 
          originEach.comment_id === newEach.comment_id
        ) === -1
      )
      if (!newComments.length) {
        showComments(useFeed, postId, { ...options, "startAt": originComments.length + per_page + 1 })
        return
      }
      setFeed(feed.map((post, index) => {
        if (index === postIdIndex) {
          return {...post, "commentsContent": [...originComments, ...newComments]}
        } else {
          return post
        }
      }))
    }, 
    //"log": "Feed.showComments", 
    signal
  })
}

const showFeed = async (useFeed, globalState, username, useSeeMorePostButton, options) => {
  const { refreshFeed, abortcontroller } = options || { "refreshFeed": null, "abortcontroller": null }
  const signal = abortcontroller ? abortcontroller.signal : null
  const [, setSeeMorePostButton] = useSeeMorePostButton
  const [feed, setFeed] = useFeed
  const startAt = refreshFeed ? 1 : feed.length + 1
  const perPage = refreshFeed ? feed.length : 5
  let path
  let attachedData
  if (globalState.profile.username) {
    attachedData = {
      "id": globalState.token, 
      "start_row": startAt, 
      "per_page": perPage
    }
    if (username) {
      if (username === globalState.profile.username) {
        path = showSelfFeedPost
      } else {
        path = viewUserFeedPost
        attachedData = { ...attachedData, username }
      }
    } else {
      path = showMixedFeedPost
    }
  } else {
    path = `${viewUserPublicFeedPost}/${username}/${startAt}/${perPage}`
  }
  await fetching(path, attachedData, {
    "callback": (response) => {
      if (response.status === 'not found post') setSeeMorePostButton('none')
      if (!response.data) return
      if (response.data.length < perPage && !refreshFeed) setSeeMorePostButton('none')
      if (response.data.length === 0) return
      // processRawData()
      let processedData = response.data.map(post => ({ ...post, "commentsContent": [] }))
      response.comment.forEach(comment => {
        processedData = processedData.map(post => {
          if (post.post_id === comment.post_id) {
            return {...post, "comments": comment.comment_number || 0}
          } else {
            return post
          }
        })
      })
      if (!feed.length || refreshFeed) {
        setFeed([...processedData])
        return
      }
      const newData = processedData.filter(newEach => 
        feed.findIndex(originEach =>
          originEach.post_id === newEach.post_id
        ) === -1
      )
      if (!newData.length) {
        showFeed(useFeed, globalState, username, useSeeMorePostButton, { ...options, "startAt": (refreshFeed ? perPage + 1 : feed.length + perPage + 1) })
        return
      }
      setFeed([...feed, ...newData])
    }, 
    //"log": "Feed.showFeed", 
    signal
  })
}

const showPost = async (useFeed, globalState, postId, options) => {
  const signal = options.abortcontroller ? options.abortcontroller.signal : null
  const [, setFeed] = useFeed
  const mainCallback = (response) => {
    if (!response.data) return
    if (response.data.length === 0) return
    // processRawData()
    let processedData = response.data.map(post => ({ ...post, "commentsContent": [] }))
    response.comment.forEach(comment => {
      processedData = processedData.map(post => {
        if (post.post_id === comment.post_id) {
          return {...post, "comments": comment.comment_number || 0}
        } else {
          return post
        }
      })
    })
    console.log('processedData')
    console.log(processedData)
    setFeed([...processedData])
  }
  const fetchSelfPost = async () => {
    await fetching(showSelfPostById, {
      "post_id": postId
    }, {
      "callback": (response) => {
        if (!response.data || response.data.length === 0) return
        mainCallback(response)
      }, 
      "log": "Feed.showSelfPost", 
      signal
    })
  }
  await fetching(viewPrivatePostById, {
    "post_id": postId
  }, {
    "callback": async (response) => {
      if (!response.data || response.data.length === 0) return
      /* bug on fetchSelfPost()
      console.log(response.data[0].username)
      console.log(globalState.profile.username)
      if (response.data[0].username === globalState.profile.username) {
        await fetchSelfPost()
      } else {
        mainCallback(response)
      }*/
      if (response.status !== 'can not view yourself') {
        mainCallback(response)
      } else {
        fetchSelfPost()
      }
    }, 
    "log": "Feed.showPost", 
    signal
  })
}

function Feed(props) {
  const { globalState } = useContext(Store)
  const useFeed = useState([])
  const [feed, ] = useFeed
  const useSeeMorePostButton = useState('block')
  const [seeMorePostButton, ] = useSeeMorePostButton
  const { postId, username } = props
  const [now, setNow] = useState(Date.now())

  useEffect(() => {
    const abortcontroller = new AbortController()
    if (!postId) {
      (async function () {
        await showFeed(useFeed, globalState, username, useSeeMorePostButton, { abortcontroller })
      }())
    }
    return () => {
      abortcontroller.abort()
    }
  }, [username])
  
  useEffect(() => {
    const timer = setTimeout(() => {
      setNow(Date.now())
    }, 1984)
    return () => {
      clearTimeout(timer)
    }
  }, [now])

  useEffect(() => {
    const abortcontroller = new AbortController()
    if (postId) {
      (async function () {
        await showPost(useFeed, globalState, postId, { abortcontroller })
      }())
    }
    return () => {
      abortcontroller.abort()
    }
  }, [postId])

  useEffect(() => {
    console.log('feed')
    console.log(feed)
  }, [feed])

  return (
    <>
      {postId && feed.length > 0 ?
        <div className="centerbackground">
          <div className="whiteframe">
            <CardItem data={feed[0]} now={now} showComments={showComments} showFeed={showPost} useFeed={useFeed} />
          </div>
        </div>
        : <></>
      }
      {!postId ?
        <div className="centerbackground">
          {props.pluseditor && globalState.token ?
            <div className="whiteframe">
              <CardEditor callback={{ showFeed, useFeed }} />
            </div>
            : <></>
          }
          {feed.length > 0 ?
            feed.map(post =>
              <div className="whiteframe" key={post.post_id}>
                <CardItem key={post.post_id} data={post} now={now} showComments={showComments} showFeed={showFeed} useFeed={useFeed} />
              </div>
            )
            : 
            <div className="whiteframe">
              <div className="card-item nothinghere">
                <img src={openroad} alt="openroad" />
              </div>
            </div>
          }
          <button onClick={() => showFeed(useFeed, globalState, username, useSeeMorePostButton)} style={{ "display": seeMorePostButton }}>See More</button>
        </div>
        : <></>
      }
    </>
  )
}

export default Feed