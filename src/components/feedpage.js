import React, { useContext } from 'react'
import { A } from 'hookrouter'
import { host } from '../_config'
import { Store } from '../_store'
import Feed from './feed'
import { Nav } from './nav'
import defaultAvatar from '../images/default_avatar.jpg'
import feedicon from '../images/feedicon.png'

function FeedPage(props) {
  const { globalState } = useContext(Store)
  const postId = props.postId
  const displayAvatar = (globalState.profile.avatar && globalState.profile.avatar !== ' ') ? `${host}${globalState.profile.avatar}` : defaultAvatar
  
  return (
    <>
      <Nav />
      <div className="feed-container">
        <div className="feed-left">
          <div>
            <A href={`/${globalState.profile.username}`} className="name">
              <img src={displayAvatar} className="avatar" alt="avatar" />
              <span>{`${globalState.profile.firstname} ${globalState.profile.lastname}`}</span>
            </A>
            <A href="/" className="link active"><img src={feedicon} className="icon" alt="icon" />Feed</A>
          </div>
        </div>
        <div className="feed-zone infeedpage">
          {postId ?
            <Feed postId={postId} />
            : <Feed pluseditor />
          }
        </div>
        <div className="feed-right">
          <div>
            <div className="language">
              English
            </div>
            <span>Fakebook &copy; 2019</span>
          </div>
        </div>
      </div>
    </>
  )
}

export default FeedPage