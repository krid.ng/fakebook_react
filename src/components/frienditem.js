import React, { useContext, useRef, useState } from 'react'
import { A, navigate } from 'hookrouter'
import { host, removefriend, requestfriend, responsefriend } from '../_config'
import { Store } from '../_store'
import { BasicModal } from './modal'
import { calculateCommentTime, fetching } from './sharedapi'
import defaultAvatar from '../images/default_avatar.jpg'
import check from '../images/check-solid.svg'

const requestFriend = async (activeUserToken, act, targetUser, useFriendStatus) => {
  const [friendStatus, setFriendStatus] = useFriendStatus
  await fetching(requestfriend, {
    "id": activeUserToken, 
    "response_username": targetUser, 
    "status": act
  }, {
    "callback": (response) => {
      if (act === 'add') {
        setFriendStatus('requesting')
      }
      if (act === 'cancel') {
        setFriendStatus('none')
      }
    }, 
    //"log": `requestFriend(${act} ${targetUser})`
  })
}

const responseFriend = async (activeUserToken, act, targetUser, useFriendStatus) => {
  const [friendStatus, setFriendStatus] = useFriendStatus
  await fetching(responsefriend, {
    "id": activeUserToken, 
    "request_username": targetUser, 
    "status": act
  }, {
    "callback": (response) => {
      if (act === 'confirm') {
        setFriendStatus('friend')
      }
      if (act === 'delete') {
        setFriendStatus('none')
      }
    }, 
    //"log": `responseFriend(${act} ${targetUser})`
  })
}

const removeFriend = async (token, targetUser, dispatch) => {
  await fetching(removefriend, {
    "id": token, 
    "username": targetUser
  }, {
    "callback": (response) => {
      if (response.status === 'remove friend complete') {
        dispatch({ 
          "type": "SETMODAL", 
          "render": false
        })
        window.location.reload()
      }
    }, 
    //"log": `removeFriend(${targetUser})`
  })
}

function FriendController(props) {
  const { globalState, dispatch } = useContext(Store)
  const { useFriendStatus, username } = props.data
  const [friendStatus, setFriendStatus] = useFriendStatus
  const [popup, setPopup] = useState('none')

  const handleBlur = () => {
    setTimeout(() => {
      setPopup('none')
    }, 200)
  }

  const handleFocus = () => {
    setPopup('block')
  }

  const handleRemove = () => {
    const message = (
      <>
        Are you sure to remove <strong>{`${username}`}</strong>?
      </>
    )
    dispatch({ 
      "type": "SETMODAL", 
      "background": 'rgba(0, 0, 0, .7)', 
      "render": () => (
        <BasicModal onClick={() => removeFriend(globalState.token, username, dispatch)} title='Remove Friend' body={message} />
      ) 
    })
  }

  return (
    <>
      {friendStatus === 'none' && globalState.token ?
        <div className="friend-controller">
          <button className="primary" onClick={() => requestFriend(globalState.token, 'add', username, useFriendStatus)}>Add</button>
        </div>
        : <></>
      }
      {friendStatus === 'requesting' && globalState.token ?
        <div className="friend-controller">
          <button onClick={() => requestFriend(globalState.token, 'cancel', username, useFriendStatus)}>Cancel</button>
        </div>
        : <></>
      }
      {friendStatus === 'pending' && globalState.token ?
        <div className="friend-controller twobuttons">
          <button className="primary" onClick={() => responseFriend(globalState.token, 'confirm', username, useFriendStatus)}>Accept</button>
          <button onClick={() => responseFriend(globalState.token, 'delete', username, useFriendStatus)}>Reject</button>
        </div>
        : <></>
      }
      {friendStatus === 'friend' && globalState.token ?
        <div className="friend-controller">
          <button onBlur={handleBlur} onFocus={handleFocus}>
            <img className="check" src={check} alt="check" /> Friend
          </button>
          <div className="popup" style={{ "display": popup }}>
            <div className="arrow"></div>
            <button onClick={handleRemove}>Remove</button>
          </div>
        </div>
        : <></>
      }
    </>
  )
}

function FriendItem(props) {
  const { firstname, lastname, profile_picture_url, status, username, id, post_owner_user_id, type, post_id, comment_id, read_status, created_at, now, state, setState } = props.data
  const displayAvatar = profile_picture_url && profile_picture_url !== ' ' ? `${host}${profile_picture_url}` : defaultAvatar
  const useFriendStatus = useState(status)
  const linkRef = useRef()
  const renderBody = (
    <>
      <a ref={linkRef} className="hidden" href={`/${username}`} />
      <img src={displayAvatar} alt={'avatar'} onClick={() => linkRef.current.click()} />
      {type ?
        <>
          <div className="name" onClick={() => navigate(`/p/${post_id}`)}>
            <A href={`/${username}`}>{`${firstname} ${lastname}`}</A>
            <span>{` ${type} ${post_id}`}</span>
          </div>
          <span className="timestamp">{calculateCommentTime(now, created_at)}</span>
        </>
        :
        <div className="name">
          <A href={`/${username}`}>{`${firstname} ${lastname}`}</A>
        </div>
      }
      <FriendController data={{ useFriendStatus, username }} />
    </>
  )
  return (
    <>
      {props.bodyClick ? 
        <li onClick={() => {
          navigate(`/${username}`)
          setState({ ...state, "friendsfocus": 'none', "searchInput": '', "notifyfocus": 'none', "settingfocus": 'none' })
        }}>
          {renderBody}
        </li>
        :
        <li>
          {renderBody}
        </li>
      }
    </>
  )
}

export { FriendController, FriendItem }