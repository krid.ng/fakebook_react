import React, { useContext, useEffect, useState } from 'react'
import { showFriendListPrivacy, showRequestingList } from '../_config'
import { Store } from '../_store'
import { FriendItem } from './frienditem'
import PrivacySelector from './privacyselector'
import { fetching } from './sharedapi'
import friends from '../images/friends.png'

const showRequestinglist = async (useRequestinglist, token, options) => {
  const { abortcontroller, useSeeMore } = options || { "abortcontroller": { "signal": null }, "useSeeMore": null }
  const signal = abortcontroller ? abortcontroller.signal : null
  const [, setSeeMore] = useSeeMore || [null, () => {}]
  const [requestinglist, setRequestinglist] = useRequestinglist
  const startAt = requestinglist.length + 1
  const perPage = 5
  await fetching(showRequestingList, {
    "id": token, 
    "row_start": startAt, 
    "per_page": perPage
  }, {
    "callback": (response) => {
      if (response.status === 'no request' || response.status === 'wrong row start') 
        setSeeMore(prevState => ({ ...prevState, "requestinglistButton": false }))
      if (!response.data) return
      if (response.data.length < perPage) 
        setSeeMore(prevState => ({ ...prevState, "requestinglistButton": false }))
      if (response.data.length === 0) return
      const newData = response.data.filter(newEach => 
        requestinglist.findIndex(originEach =>
          originEach.username === newEach.username
        ) === -1
      )
      setRequestinglist([...requestinglist, ...newData])
    }, 
    //"log": "FriendResponse.showRequestinglist", 
    signal
  })
}

function FriendList(props) {
  const useStore = useContext(Store)
  const { globalState } = useStore
  const { showFriendlist, useFriendlist, username } = props
  const isMe = username === globalState.profile.username
  const user = isMe ? 'self' : username
  const [friendlist, ] = useFriendlist
  const useFriendlistPrivacy = useState('')
  const [friendlistPrivacy, setFriendlistPrivacy] = useFriendlistPrivacy
  const useRequestinglist = useState([])
  const [requestinglist, ] = useRequestinglist
  const useSeeMore = useState({
    "friendlistButton": true, 
    "requestinglistButton": true
  })
  const [{ friendlistButton, requestinglistButton }, ] = useSeeMore
  
  useEffect(() => {
    const abortcontroller = new AbortController();
    (async function () {
      if (isMe) {
        await showRequestinglist(useRequestinglist, globalState.token, { useSeeMore, abortcontroller })
        await fetching(showFriendListPrivacy, {
          "id": globalState.token
        }, {
          "callback": (response) => {
            setFriendlistPrivacy(response.privacy)
          }
        })
      }
    } ())
    return () => {
      abortcontroller.abort()
    }
  }, [])

  return (
    <div className="profile-friends">
      <div className="title">
        <img src={friends} alt="icon" />
        <h2>Friends</h2>
        {isMe ?
          <PrivacySelector data={{ "privacy": friendlistPrivacy, setFriendlistPrivacy, "useFor": "friendlist" }} />
          : <></>
        }
      </div>
      {requestinglist.length ?
        <ul className="content">
          {requestinglist.map(each => <FriendItem key={each.username} data={{ ...each, "status": "requesting" }} />)}
          {requestinglistButton ?
            <li className="button" onClick={() => showRequestinglist(useRequestinglist, globalState.token, { useSeeMore })}>See more Invitations</li>
            : <></>
          }
        </ul>
        : <></>
      }
      <ul className="content">
        {friendlist.length ?
          friendlist.map(each => <FriendItem key={each.username} data={{ ...each, "status": "friend" }} />)
          : <></>
        }
        {friendlistButton ?
          <li className="button" onClick={() => showFriendlist(useFriendlist, globalState.token, user, { useSeeMore })}>See more Friends</li>
          : <></>
        }
      </ul>
    </div>
  )
}

export default FriendList