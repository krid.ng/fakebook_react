import React, { useContext, useEffect, useRef, useState } from 'react'
import { A } from 'hookrouter'
import { showPendingList, showRecommendedList } from '../_config'
import { Store } from '../_store'
import { FriendItem } from './frienditem'
import { fetching } from './sharedapi'
import friendsicon from '../images/friends.svg'

const showPendinglist = async (usePendinglist, token, options) => {
  const { abortcontroller, useSeeMore } = options
  const signal = abortcontroller ? abortcontroller.signal : null
  const [, setSeeMore] = useSeeMore || [null, () => {}]
  const [pendinglist, setPendinglist] = usePendinglist
  const startAt = pendinglist.length ? pendinglist.length + 1 : 1
  const perPage = 5
  await fetching(showPendingList, {
    "id": token, 
    "row_start": startAt, 
    "per_page": perPage
  }, {
    "callback": (response) => {
      if (response.status === 'no request' || response.status === 'wrong row start') 
        setSeeMore(prevState => ({ ...prevState, "pendinglistButton": false }))
      if (!response.data) return
      if (response.data.length < perPage) 
        setSeeMore(prevState => ({ ...prevState, "pendinglistButton": false }))
      if (response.data.length === 0) return
      const newData = response.data.filter(newEach => 
        pendinglist.findIndex(originEach =>
          originEach.username === newEach.username
        ) === -1
      )
      setPendinglist([...pendinglist, ...newData])
    }, 
    //"log": "FriendResponse.showPendinglist", 
    signal
  })
}

const showRecommendedlist = async (useRecommendedlist, token, options) => {
  const { abortcontroller, useSeeMore } = options
  const signal = abortcontroller ? abortcontroller.signal : null
  const [, setSeeMore] = useSeeMore || [null, () => {}]
  const [recommendedlist, setRecommendedlist] = useRecommendedlist
  const startAt = recommendedlist.length ? recommendedlist.length + 1 : 1
  const perPage = 5
  await fetching(showRecommendedList, {
    "id": token, 
    "row_start": startAt, 
    "per_page": perPage
  }, {
    "callback": (response) => {
      if (response.status === 'no friend recommend' || response.status === 'wrong row start') 
        setSeeMore(prevState => ({ ...prevState, "recommendedlistButton": false }))
      if (!response.data) return
      if (response.data.length < perPage) 
        setSeeMore(prevState => ({ ...prevState, "recommendedlistButton": false }))
      if (response.data.length === 0) return
      const newData = response.data.filter(newEach => 
        recommendedlist.findIndex(originEach =>
          originEach.username === newEach.username
        ) === -1
      )
      setRecommendedlist([...recommendedlist, ...newData])
    }, 
    //"log": "FriendResponse.showRecommendedlist", 
    signal
  })
}

function FriendResponse(props) {
  const { globalState } = useContext(Store)
  const { useNavState, toggleFocus, usePendinglist } = props.data
  const searching = props.searching
  const [navState, setNavState] = useNavState
  const { friendsalert, friendsfocus } = navState
  const [pendinglist, ] = usePendinglist
  const useRecommendedlist = useState([])
  const [recommendedlist,] = useRecommendedlist
  const useSeeMore = useState({
    "pendinglistButton": true, 
    "recommendedlistButton": true
  })
  const [{ pendinglistButton, recommendedlistButton }, ] = useSeeMore
  const friendRef = useRef()

  useEffect(() => {
    if (!globalState.token) return
    const abortcontroller = new AbortController();
    (async function () {
      await showPendinglist(usePendinglist, globalState.token, { abortcontroller, useSeeMore })
      await showRecommendedlist(useRecommendedlist, globalState.token, { abortcontroller, useSeeMore })
    } ())
    return () => {
      abortcontroller.abort()
    }
  }, [globalState.token])

  return (
    <div ref={friendRef} name="friends" className={`friends ${searching}`}>
      <A className="icon" onClick={toggleFocus} href="#">
        <span className="notify">{friendsalert > 0 ? friendsalert : ''}</span>
        <img className="friendsicon" src={friendsicon} alt="Friends" />
        <div className="arrowup13" style={{"display": friendsfocus}}></div>
      </A>
      <div className="widepanel" style={{"display": friendsfocus}}>
        <div className="title">
          <h2>Friend Requests</h2>
        </div>
        <ul className="content">
          {pendinglist.length ?
            pendinglist.map(each => <FriendItem key={each.username} data={{ ...each, "status": "pending" }} />)
            : <li className="nodata">There is no request recently.</li>
          }
          {pendinglistButton ?
            <li className="button" onClick={() => showPendinglist(usePendinglist, globalState.token, { useSeeMore })}>See more Incoming requests</li>
            : <></>
          }
        </ul>
        <div className="title">
          <h2>Friend Recommended</h2>
        </div>
        <ul className="content">
          {recommendedlist.length ?
            recommendedlist.map(each => <FriendItem key={each.username} data={{ ...each, "status": "none" }} />)
            : <li className="nodata">We will try to find someone for you.</li>
          }
          {recommendedlistButton ?
            <li className="button" onClick={() => showRecommendedlist(useRecommendedlist, globalState.token, { useSeeMore })}>See more Recommended</li>
            : <></>
          }
        </ul>
      </div>
    </div>
  )
}

export default FriendResponse