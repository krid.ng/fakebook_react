import React, { useContext, useEffect } from 'react'
import { Store } from '../_store'
import { CloseButton } from './svg'

function BasicModal(props) {
  const { dispatch } = useContext(Store)

  return (
    <>
      <div className="basictitle">
        {props.title}
      </div>
      <div className="basicbody">
        {props.body}
      </div>
      <div className="basicfoot">
        <button onClick={() => dispatch({ "type": "SETMODAL", "render": false })}>Cancel</button>
        <button onClick={() => props.onClick()}>Confirm</button>
      </div>
      <CloseButton color='#ccd0d5' size='12' onClick={() => dispatch({ "type": "SETMODAL", "render": false })} />
    </>
  )
}

function Modal(props) {
  const authstyle = props.authstyle || false
  const background = props.background || 'rgba(255, 255, 255, 0.8)'
  const render = props.render || (() => {})

  /*useEffect(() => {
    console.log(props.authstyle)
    console.log(authstyle)
  }, [props.authstyle])*/

  return (
    <div className={authstyle ? 'auth-signupmodal' : 'modal-overlay'} style={{ background }}>
      <div className={authstyle ? 'signupbox' : 'modalframe'}>
        {render()}
      </div>
    </div>
  )
}

export { BasicModal, Modal }