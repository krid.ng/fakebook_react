import React, { useContext, useEffect, useState } from 'react'
import { A } from 'hookrouter'
//import io from 'socket.io-client'
import { clearNotifyNumber, host, logoutPath, search, showNotifyList } from '../_config'
import { Store } from '../_store'
import { handleLogIn } from './authpage'
import { FriendItem } from './frienditem'
import FriendResponse from './friendresponseinnav'
import { fetching } from './sharedapi'
import { BackButton, CloseButton, SearchButton } from './svg.js'
import bellicon from '../images/bell.svg'
import defaultAvatar from '../images/default_avatar.jpg'
import loading from '../images/loading.gif'
import logo from '../images/icon.svg'

let currentSearchText = ''

function Nav() {
  //const socket = io()
  const { globalState, dispatch } = useContext(Store)
  const displayAvatar = (globalState.profile.avatar && globalState.profile.avatar !== ' ') ? `${host}${globalState.profile.avatar}` : defaultAvatar
  const usePendinglist = useState([])
  const [pendinglist,] = usePendinglist
  const useNavState = useState({
    friendsalert: 0, 
    friendsfocus: 'none', 
    loading: false,
    notifyalert: 0, 
    notifyfocus: 'none', 
    notifylist: [],
    settingfocus: 'none',
    searching: '',
    searchInput: '',
    searchResult: []
  })
  const [state, setState] = useNavState
  const { searching, searchInput } = state
  const { friendsalert, friendsfocus, notifyalert, notifyfocus, notifylist, settingfocus } = state
  
  const handleBlur = (name) => {
    setTimeout(() => {
      setState({ ...state, [`${name}focus`]: 'none' })
    }, 200)
  }

  const handleFocus = (name) => {
    setState({...state, [`${name}focus`]: 'block', "friendsfocus": "none", "notifyfocus": "none"})
  }

  const handleLogOut = async () => {
    try {
      await fetch(logoutPath, {
        "method": "post",
        "credentials": 'include', 
        "headers": {
          "content-type": "application/json"
        },
        "body": JSON.stringify({
          "id": globalState.token
        })
      })
      dispatch({ type: 'LOGOUT' })
      window.locatiom.reload()
    } catch (err) {
      console.log(`Nav.handleLogOut: ${err}`)
    }
  }

  const handleSearch = async (searchInput, signal) => {
    await fetching(search, {
      "fullname": searchInput,
      "start_row": 1,
      "per_page": 10
    }, {
      "callback": (response) => {
        setState({ ...state, "searchResult": response.data, "loading": false })
      },
      //"log": `Nav.handleSearch: ${searchInput}`,
      signal
    })
  }

  const handleSearchInput = (e) => {
    setState({ ...state, "searchInput": e.target.value })
  }

  const toggleFocus = () => {
    if (state.friendsfocus === 'block') {
      setState({ ...state, "friendsfocus": "none" })
    } else {
      setState({ ...state, "friendsalert": '', "friendsfocus": "block", "notifyfocus": "none" })
    }
  }

  const toggleNotify = () => {
    if (state.notifyfocus === 'block') {
      setState({ ...state, "notifyfocus": "none" })
    } else {
      setState({ ...state, "notifyalert": '', "notifyfocus": "block", "friendsfocus": "none" })
    }
  }

  const toggleSearch = () => {
    if (searching === 'search') {
      setState({ ...state, "searching": '' })
    } else {
      setState({...state, "searching": 'search', "friendsfocus": "none", "notifyfocus": "none" })
    }
  }

  useEffect(() => {
    let didCancelUseEffect = false
    if (!didCancelUseEffect) {
      setState(prevState => ({ ...prevState, "friendsalert": pendinglist.length }))
    }
    return () => {
      didCancelUseEffect = true
    }
  }, [pendinglist])

  useEffect(() => {
    const abortcontroller = new AbortController()
    currentSearchText = state.searchInput;
    (async function(){
      if (state.searchInput.trim() === '') {
        setState({ ...state, "searchResult": [] })
        return
      }
      setTimeout(async () => {
        if (currentSearchText === state.searchInput) {
          setState({ ...state, "loading": true })
          await handleSearch(state.searchInput, abortcontroller.signal)
        }
      }, 500)
    }())
    return () => {
      abortcontroller.abort()
    }
  }, [state.searchInput])

  return (
    <nav>
      <div className="nav-bar">
        <A className={`logo ${searching}`} href="/">
          <img className="logo" src={logo} alt="logo" />
        </A>
        <div className={`search-panel ${searching}`}>
          <A className={`searchbutton`} onClick={toggleSearch} href="#">
            {searching === 'search' ?
              <BackButton color={'#2e487d'} size={22} width={4} />
              : <SearchButton color={'#2e487d'} />
            }
          </A>
          <input className={`input ${searching}`} value={state.searchInput} onChange={handleSearchInput} placeholder="Search" />
          {state.loading ? 
            <img className="loading" src={loading} alt="loading" />
            : 
            <>
              {searchInput.length ? 
                <CloseButton color={'#999999'} size={14} onClick={() => setState({ ...state, "searchInput": '' })} />
                : <></>
              }
            </>
          }
          {state.searchResult !== undefined ?
            <ul className={`result ${searching}`}>
              {state.searchResult.map(person => (
                <FriendItem bodyClick data={{ ...person, state, setState, "status": '' }} key={person.username} />
              ))}
            </ul>
            : <></>
          }
        </div>
        <A className={`text profile ${searching}`} href="/profile">
          <img className="avatar" src={displayAvatar} alt="avatar" />
          <span>{globalState.profile.firstname}</span>
        </A>
        <span className={`verticalline ${searching}`} />
        <A className={`home ${searching}`} href="/">
          Home
        </A>
        <span className={`verticalline ${searching}`} />
        <FriendResponse data={{useNavState, toggleFocus, usePendinglist}} searching={searching} />
        <Notify data={{useNavState, toggleNotify}} searching={searching} />
        <div name="setting" className={`setting ${searching}`} onFocus={() => handleFocus('setting')} onBlur={() => handleBlur('setting')}>
          <A className="icon" href="#">
            <div className="arrowdown" />
            <div className="arrowup6" style={{"display": settingfocus}} />
          </A>
          <div className="panel" style={{"display": settingfocus}}>
            {/*<button>Preference</button>*/}
            <button onClick={handleLogOut}>Log Out</button>
          </div>
        </div>
      </div>
    </nav>
  )
}

function NavForGuest() {
  const { dispatch } = useContext(Store)
  const [info, setInfo] = useState({
    "loginemail": '', 
    "loginpassword": ''
  })
  const { loginemail, loginpassword } = info

  const handleChange = (e) => {
    setInfo({ ...info, [e.target.id]: e.target.value })
  }

  return (
    <nav>
      <div className="nav-bar">
        <A className="logo" href="/">
          <img className="logo" src={logo} alt="logo" />
        </A>
        <form onSubmit={() => handleLogIn(null, [loginemail, loginpassword], dispatch)}>
          <input id="loginemail" className="auth" type="email" placeholder="E-mail" required
            value={loginemail} onChange={handleChange}
          />
          <input id="loginpassword" className="auth" type="password" placeholder="Password" required
            value={loginpassword} onChange={handleChange}
          />
          <button id="loginsubmit" className="auth" type="submit">Login</button>
        </form>
      </div>
    </nav>
  )
}

const showNotifylist = async (useNavState, token, options) => {
  const { abortcontroller, useSeeMore } = options
  const signal = abortcontroller ? abortcontroller.signal : null
  const [, setSeeMore] = useSeeMore || [null, () => {}]
  const [state, setState] = useNavState
  const { notifylist } = state
  const startAt = notifylist.length ? notifylist.length + 1 : 1
  const perPage = 5
  await fetching(showNotifyList, {
    "id": token, 
    "row_start": startAt, 
    "per_page": perPage
  }, {
    "callback": (response) => {
      console.log(response)
      if (response.status === 'not found notification') 
        setSeeMore(prevState => ({ ...prevState, "notifylistButton": false }))
      if (!response.data) return
      if (response.data.length < perPage) 
        setSeeMore(prevState => ({ ...prevState, "notifylistButton": false }))
      if (response.data.length === 0) return
      const newData = response.data.filter(newEach => 
        notifylist.findIndex(originEach =>
          originEach.username === newEach.username
        ) === -1
      )
      setState({ ...state, "notifylist": [...notifylist, ...newData] })
    }, 
    //"log": "FriendResponse.showNotifylist", 
    signal
  })
}

function Notify(props) {
  const { globalState } = useContext(Store)
  const { useNavState, toggleNotify } = props.data
  const searching = props.searching
  const [state, setState] = useNavState
  const { notifyalert, notifyfocus } = state
  const useNotifylist = useState([])
  const [notifylist,] = useNotifylist
  const useSeeMore = useState({
    "notifylistButton": true
  })
  const [{ notifylistButton }, ] = useSeeMore
  const [now, setNow] = useState(Date.now())

  const clearNotify = async () => {
    await fetching(clearNotifyNumber, {}, {
      "callback": () => {
        setState({ ...state, "notifyalert": 0 })
      }, 
      //"log": "Nav.clearNotifyNumber", 
    })
  }

  useEffect(() => {
    const timer = setTimeout(() => {
      setNow(Date.now())
    }, 1984)
    return () => {
      clearTimeout(timer)
    }
  }, [now])

  useEffect(() => {
    if (!globalState.profile.username) return
    const abortcontroller = new AbortController();
    (async function () {
      await showNotifylist(useNavState, globalState.token, { abortcontroller, useSeeMore })
    } ())
    return () => {
      abortcontroller.abort()
    }
  }, [])

  useEffect(() => {
    console.log('notifylist')
    console.log(notifylist)
  }, [notifylist])

  return (
    <div name={`notify ${searching}`} className="notify">
      <A className="icon" onClick={toggleNotify} href="#">
        <span className="notify">{notifyalert > 0 ? notifyalert : ''}</span>
        <img className={`bellicon ${searching}`} src={bellicon} alt="Notification" />
        <div className="arrowup13" style={{"display": notifyfocus}}></div>
      </A>
      <div className="widepanel" style={{"display": notifyfocus}}>
        <div className="title">
          <h2>Notification Panel</h2>
          <A className="clearall" onClick={() => clearNotify()} href="#">marks all as read</A>
        </div>
        <ul className="content">
          {notifylist.length ?
            notifylist.map(each => <FriendItem key={each.id} data={{ ...each, now, "status": '' }} />)
            : <li className="nodata">There is nothing.</li>
          }
          {notifylistButton ?
            <li className="button" onClick={() => showNotifylist(useNavState, globalState.token, { useSeeMore })}>See past notifies</li>
            : <></>
          }
        </ul>
      </div>
    </div>
  )
}

export { Nav, NavForGuest }