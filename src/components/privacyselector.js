import React, { useContext, useState } from 'react'
import { editFriendListPrivacy, editSelfProfilePrivacy, editSinglePostPrivacy } from '../_config'
import { Store } from '../_store'
import { fetching } from './sharedapi'
import { Check, Friends, Globe, Private, } from './svg'

function PrivacySelector(props) {
  const { globalState } = useContext(Store)
  const useFor = props.data.useFor
  const name = props.data.name || null
  const post_id = props.data.post_id || null
  const privacy = props.data.privacy || null
  const setFriendlistPrivacy = props.data.setFriendlistPrivacy || (() => {})
  const setPrivacy = props.data.setPrivacy || (() => {})
  const checked = <Check className="active privacyicon" />
  const checkedPublic = privacy === 'public' ? checked : <></>
  const checkedFriends = privacy === 'friend' ? checked : <></>
  const checkedPrivate = privacy === 'only me' ? checked : <></>
  const [list, setList] = useState('none')

  const changePrivacy = async (newPrivacy) => {
    if (useFor === 'cardeditor') {
      setPrivacy(newPrivacy)
    }
    if (useFor === 'carditem') {
      await fetching(editSinglePostPrivacy, {
        post_id, 
        "username": globalState.profile.username, 
        "privacy": newPrivacy
      }, {
        "callback": (response) => {
          window.location.reload()
        }, 
        //"log": `CardItem.setPrivacy (post: ${post_id})`
      })
    }
    if (useFor === 'friendlist') {
      await fetching(editFriendListPrivacy, {
        "id": globalState.token, 
        "privacy": newPrivacy
      }, {
        "callback": (response) => {
          if (response.status)
            setFriendlistPrivacy(newPrivacy)
        }, 
        "log": `CardItem.setPrivacy (friendlist)`
      })
    }
    if (useFor === 'profileitem') {
      await fetching(editSelfProfilePrivacy, {
        "id": globalState.token, 
        [name]: newPrivacy
      }, {
        "callback": (response) => {
          window.location.reload()
        }, 
        //"log": `CardItem.setPrivacy (profile.${name})`
      })
    }
  }

  const handleBlur = () => {
    setTimeout(() => {
      setList('none')
    }, 200)
  }

  const handleFocus = () => {
    setList('block')
  }

  return (
    <>
      {privacy ?
        <div className="privacy-selector">
          <button className="current" onFocus={handleFocus} onBlur={handleBlur}>
            {privacy === 'public' ?
              <>
                <Globe />
                <span className="label">Public</span>
                <div className="arrowdown" />
              </>
              : <></>
            }
            {privacy === 'friend' ? 
              <>
                <Friends />
                <span className="label">Friends</span>
                <div className="arrowdown" />
              </>
              : <></>
            }
            {privacy === 'only me' ? 
              <>
                <Private />
                <span className="label">Only Me</span>
                <div className="arrowdown" />
              </>
              : <></>
            }
          </button>
          <div className="list" style={{ "display": list }}>
            <span className="description">Who can see this ?</span>
            <button onClick={() => changePrivacy('public')}>{checkedPublic}<Globe /> Public</button>
            <button onClick={() => changePrivacy('friend')}>{checkedFriends}<Friends /> Friends</button>
            <button onClick={() => changePrivacy('only me')}>{checkedPrivate}<Private /> Only Me</button>
          </div>
        </div>
        : <></>
      }
    </>
  )
}

export default PrivacySelector