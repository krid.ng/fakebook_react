import React, { useContext, useEffect, useRef, useState } from 'react'
import { A } from 'hookrouter'
import { checkFriendStatus, editSelfProfile, host, showAlbumPostPictures, showFriendList, showSelfProfile, showSelfProfilePrivacy, viewPrivateFriendList, viewPrivateProfile, viewPublicFriendList, viewPublicProfile } from '../_config'
import { Store } from '../_store'
import Feed from './feed'
import { FriendController } from './frienditem'
import FriendList from './friendlistinprofile'
import { Nav, NavForGuest } from './nav'
import PrivacySelector from './privacyselector'
import { fetching } from './sharedapi'
import { Camera } from './svg'
import about from '../images/about.png'
import defaultAvatar from '../images/default_avatar.jpg'
import defaultCover from '../images/worldwidenetwork.png'
import { navigate } from 'hookrouter/dist/router'

const showFriendlist = async (useFriendlist, token, username, options) => {
  const setShowFriendMenu = options.setShowFriendMenu || (() => {})
  const { abortcontroller, setPublic, useSeeMore } = options
  const signal = abortcontroller ? abortcontroller.signal : null
  const [, setSeeMore] = useSeeMore || [null, () => {}]
  const [friendlist, setFriendlist] = useFriendlist || [null, () => {}]
  const startAt = friendlist ? friendlist.length + 1 : 1
  const perPage = 10

  const callback = (response) => {
    if (response.status === 'no friend' || response.status === 'wrong row start') 
      setSeeMore(prevState => ({ ...prevState, "friendlistButton": false }))
    if (!response.data) return
    if (response.data.length < perPage) 
      setSeeMore(prevState => ({ ...prevState, "friendlistButton": false }))
    if (response.data.length === 0) return
    const newData = response.data.filter(newEach => 
      friendlist.findIndex(originEach =>
        originEach.username === newEach.username
      ) === -1
    )
    setFriendlist([...friendlist, ...newData])
    if (response.data) {
      setShowFriendMenu('inline-block')
    } else {
      setShowFriendMenu('none')
    }
  }
  if (username === 'self') {
    await fetching(showFriendList, {
      "id": token, 
      "row_start": startAt, 
      "per_page": perPage
    }, {
      callback, 
      //"log": "Friends.showFriendlist", 
      signal
    })
    setShowFriendMenu('inline-block')
    return
  }
  if (username && setPublic) {
    await fetching(viewPrivateFriendList, {
      "id": token, 
      username, 
      "start_row": startAt, 
      "per_page": perPage
    }, {
      callback, 
      //"log": `Profile.viewPrivateFriendList [${user}]`, 
      signal
    })
  } else {
    await fetching(`${viewPublicFriendList}/${username}/${startAt}/${perPage}`, null, {
      callback, 
      //"log": `Profile.viewPublicFriendList [${user}]`, 
      signal
    })
  }
}

const fetchProfile = async ({ user, useProfile, useStore, abortcontroller, setFriendStatus, setShowFriendMenu, useFriendlist }) => {
  if (!user) return
  const [, setProfile] = useProfile
  const { globalState, dispatch } = useStore
  const isMe = user === globalState.profile.username
  const signal = abortcontroller ? abortcontroller.signal : null
  const callback = (response) => {
    const { bio, birthday, come_from, cover_picture_url, education, email, firstname, gender, hobbies, lastname, lives, profile_picture_url, relationship_status, username, work_place } = response
    let newProfile
    if (isMe) {
      newProfile = {
        "avatar": profile_picture_url,
        "bio": bio || '',
        "birthday": globalState.profile.birthday,
        "come_from": come_from || '',
        "cover": cover_picture_url,
        "education": education || '',
        "email": globalState.profile.email,
        "firstname": globalState.profile.firstname,
        "gender": globalState.profile.gender,
        "hobbies": hobbies || '',
        "lastname": globalState.profile.lastname,
        "lives": lives || '',
        "relationship_status": relationship_status || '',
        "username": globalState.profile.username,
        "work_place": work_place || ''
      }
      dispatch({ "type": "EDITPROFILE", "profile": { ...newProfile } })
    } else {
      newProfile = {
        "avatar": profile_picture_url,
        "bio": bio || '',
        "birthday": birthday || '',
        "come_from": come_from || '',
        "cover": cover_picture_url,
        "education": education || '',
        "email": email || '',
        "firstname": firstname || '',
        "gender": gender || '',
        "hobbies": hobbies || '',
        "lastname": lastname || '',
        "lives": lives || '',
        "relationship_status": relationship_status || '',
        "username": username || '',
        "work_place": work_place || ''
      }
    }
    setProfile({ ...newProfile })
  }
  let loggedIn = globalState.profile.username
  let friendstatus = 'none'
  if (!loggedIn) {
    await fetching(`${viewPublicProfile}/${user}`, null, {
      callback, 
      //"log": `Profile.viewPublicProfile [${user}]`, 
      signal
    })
    await showFriendlist(useFriendlist, globalState.token, user, { abortcontroller, "public": true, setShowFriendMenu })
    return
  }
  if (isMe) {
    await fetching(showSelfProfile, {
      "id": globalState.token
    }, {
      callback, 
      //"log": `Profile.showSelfProfile`, 
      signal
    })
    await showFriendlist(useFriendlist, globalState.token, 'self', { abortcontroller, setShowFriendMenu })
    return
  }
  await fetching(checkFriendStatus, {
    "id": globalState.token,
    "username": user
  }, {
    "callback": (response) => {
      friendstatus = response.status
    }, 
    //"log": `checkFriendStatus`, 
    signal
  })
  setFriendStatus(friendstatus)
  if (friendstatus === 'friend') {
    await fetching(viewPrivateProfile, {
      "id": globalState.token, 
      "username": user
    }, {
      callback, 
      //"log": `Profile.viewPrivateProfile`, 
      signal
    })
    await showFriendlist(useFriendlist, globalState.token, user, { abortcontroller, setShowFriendMenu })
  } else {
    await fetching(`${viewPublicProfile}/${user}`, null, {
      callback, 
      //"log": `Profile.viewPublicProfile [${user}]`, 
      signal
    })
    await showFriendlist(useFriendlist, globalState.token, user, { abortcontroller, "setPublic": true, setShowFriendMenu })
  }
}

const showAlbum = async (album, setAlbum, signal) => {
  await fetching(showAlbumPostPictures, {
    "start_row": 1,
    "per_page": 18
  }, {
    "callback": (response) => {
      if (!response) return
      setAlbum(response.post_picture_album)
    }, 
    //"log": `showAlbum`, 
    signal
  })
}

function Profile(props) {
  const useStore = useContext(Store)
  const { globalState } = useStore
  const user = props.username
  const isMe = user === globalState.profile.username
  const useProfile = useState({})
  const [profile, ] = useProfile
  const useFriendStatus = useState('none')
  const [, setFriendStatus] = useFriendStatus
  const useView = useState(props.view)
  const [view, ] = useView
  const [showFriendMenu, setShowFriendMenu] = useState('none')
  const useFriendlist = useState([])
  const [album, setAlbum] = useState([])
  const fetchProfilePackage = { user, useProfile, useStore, setFriendStatus, setShowFriendMenu }

  useEffect(() => {
    const abortcontroller = new AbortController();
    (async function() {
      await fetchProfile({ ...fetchProfilePackage, abortcontroller, useFriendlist })
      if (globalState.profile.username === user) 
        await showAlbum(album, setAlbum, abortcontroller.signal)
    } ())
    return () => {
      abortcontroller.abort()
    }
  }, [user])

  return (
    <>
      { globalState.token ? <Nav /> : <NavForGuest /> }
      <ProfileHeader isMe={isMe} useFriendStatus={useFriendStatus} showFriendMenu={showFriendMenu} profile={profile} useView={useView} />
      {view === 'timeline' ?
        <div className="time-line">
          <div className="left">
            <ul className="content">
              <ProfileItem data={{ "head": "Biography", "name": "bio", fetchProfilePackage }} editable={profile.username === globalState.profile.username} />
            </ul>
            {album ?
              <>
                {album.length > 0 ?
                  <div className="album">
                    <h3>Album</h3>
                    {album.map(pic => <img src={`${host}${pic.picture_url}`} alt={pic.post_id} /*onClick={() => navigate(`/p/${pic.post_id}`)}*/ />)}
                  </div>
                  : <></>
                }
              </>
              : <></>
            }
            <div className="language">
              English
            </div>
            <span className="misc">Fakebook &copy; 2019</span>
          </div>
          <div className="right">
            <Feed username={user} />
          </div>
        </div>
        : <></>
      }
      {view === 'about' ?
        <About isMe={isMe} useProfile={useProfile} fetchProfilePackage={fetchProfilePackage} />
        : <></>
      }
      {view === 'friends' ?
        <FriendList showFriendlist={showFriendlist} useFriendlist={useFriendlist} username={user} />
        : <></>
      }
    </>
  )
}

function About(props) {
  const { globalState } = useContext(Store)
  const { isMe, useProfile, fetchProfilePackage } = props
  const [profile, ] = useProfile
  const [minorView, setMinorView] = useState(profile.bio || isMe ? 'Overview' : 'Contacts and Information')
  const editable = profile.username === globalState.profile.username
  const activeStyle = {
    "borderLeft": "3px solid #3b5998", 
    "color": "black", 
    "fontWeight": "bold"
  }
  const [privacy, setPrivacy] = useState({
    "bio": "",
    "lives": "",
    "come_from": "",
    "work_place": "",
    "education": "",
    "relationship_status": "",
    "hobbies": "",
    "birthday": "",
    "email": ""
  })

  const handleMinorView = (e) => {
    setMinorView(e.target.innerText)
  }

  useEffect(() => {
    const abortcontroller = new AbortController();
    if (isMe) {
      (async function() {
        await fetching(showSelfProfilePrivacy, {
          "id": globalState.token
        }, {
          "callback": (response) => {
            setPrivacy({ ...response })
          }, 
          //"log": `About.showSelfProfilePrivacy`, 
          "signal": abortcontroller.signal
        })
      } ())
    }
    return () => {
      abortcontroller.abort()
    }
  }, [])

  return (
    <div className="profile-about">
      <div className="title">
        <img src={about} alt="icon" />
        <h2>About</h2>
      </div>
      <div className="frame">
        {isMe || profile.bio ?
          <>
            <div className="topic" onClick={handleMinorView}>
              <div style={minorView === 'Overview' ? activeStyle : {}}>
                Overview
              </div>
            </div>
            {minorView === 'Overview' ?
              <ul className="content">
                <ProfileItem data={{ "head": "Biography", "name": "bio", fetchProfilePackage, "privacy": privacy.bio }} editable={editable} />
              </ul>
              : <></>
            }
          </>
          : <></>
        }
        {isMe || profile.education || profile.work_place ?
          <>
            <div className="topic" onClick={handleMinorView}>
              <div style={minorView === 'Workplace and Education' ? activeStyle : {}}>
                Workplace and Education
              </div>
            </div>
            {minorView === 'Workplace and Education' ?
              <ul className="content">
                {isMe || profile.work_place ?
                  <ProfileItem data={{ "head": "Works at", "name": "work_place", fetchProfilePackage, "privacy": privacy.work_place }} editable={editable} />
                  : <></>
                }
                {isMe || profile.education ?
                  <ProfileItem data={{ "head": "Education", "name": "education", fetchProfilePackage, "privacy": privacy.education }} editable={editable} />
                  : <></>
                }
              </ul>
              : <></>
            }
          </>
          : <></>
        }
        {isMe || profile.come_from || profile.lives ?
        <>
          <div className="topic" onClick={handleMinorView}>
            <div style={minorView === 'Living' ? activeStyle : {}}>
              Living
            </div>
          </div>
          {minorView === 'Living' ?
            <ul className="content">
              {isMe || profile.come_from ?
                <ProfileItem data={{ "head": "Comes from", "name": "come_from", fetchProfilePackage, "privacy": privacy.come_from }} editable={editable} />
                : <></>
              }
              {isMe || profile.lives ?
                <ProfileItem data={{ "head": "Lives in", "name": "lives", fetchProfilePackage, "privacy": privacy.lives }} editable={editable} />
                : <></>
              }
            </ul>
            : <></>
          }
        </>
        : <></>
        }
        {isMe || profile.email || profile.birthday || profile.gender ?
        <>
          <div className="topic" onClick={handleMinorView}>
            <div style={minorView === 'Contacts and Information' ? activeStyle : {}}>
              Contacts and Information
            </div>
          </div>
          {minorView === 'Contacts and Information' ?
            <ul className="content">
              {isMe || profile.email ?
                <ProfileItem data={{ "head": "Contacts", "label": "E-mail", "name": "email", fetchProfilePackage, "privacy": privacy.email }} editable={false} />
                : <></>
              }
              {isMe || profile.birthday ?
                <ProfileItem data={{ "head": "Basic Info", "label": "Birthday", "name": "birthday", fetchProfilePackage, "privacy": privacy.birthday }} editable={false} />
                : <></>
              }
              {isMe || profile.gender ?
                <>
                  {profile.birthday ?
                    <ProfileItem data={{ "label": "Gender", "name": "gender", fetchProfilePackage, "privacy": privacy.gender }} editable={false} />
                    : <ProfileItem data={{ "head": "Basic Info", "label": "Gender", "name": "gender", fetchProfilePackage, "privacy": privacy.gender }} editable={false} />
                  }
                </>
                : <></>
              }
            </ul>
            : <></>
          }
        </>
        : <></>
        }
        {isMe || profile.relationship_status ?
        <>
          <div className="topic" onClick={handleMinorView}>
            <div style={minorView === 'Family' ? activeStyle : {}}>
              Family
            </div>
          </div>
          {minorView === 'Family' ?
            <ul className="content">
              <ProfileItem data={{ "head": "Marriage Status", "name": "relationship_status", fetchProfilePackage, "privacy": privacy.relationship_status }} editable={editable} />
            </ul>
            : <></>
          }
        </>
        : <></>
        }
        {isMe || profile.hobbies ?
        <>
          <div className="topic" onClick={handleMinorView}>
            <div style={minorView === 'Personal' ? activeStyle : {}}>
              Personal
            </div>
          </div>
          {minorView === 'Personal' ?
            <ul className="content">
              <ProfileItem data={{ "head": "Hobbies", "name": "hobbies", fetchProfilePackage, "privacy": privacy.hobbies }} editable={editable} />
            </ul>
            : <></>
          }
        </>
        : <></>
        }
      </div>
    </div>
  )
}

function ProfileHeader(props) {
  const { globalState, dispatch } = useContext(Store)
  const { avatar, cover, firstname, lastname, username } = props.profile
  const displayAvatar = (avatar && avatar !== ' ') ? `${host}${avatar}` : defaultAvatar
  const displayCover = (cover && cover !== ' ') ? `${host}${cover}` : defaultCover
  const [view, setView] = props.useView
  const { isMe, showFriendMenu, useFriendStatus } = props
  const avatarRef = useRef()
  const coverRef = useRef()
  const activeStyle = {
    "color": '#4b4f56', 
    "cursor": 'default'
  }
  const checkView = (condition) => (
    condition ? {"display": 'block'} : {"display": 'none'}
  )

  const handleUploadPicture = async (e, ref) => {
    if (!ref.current.files.length) return
    const name = ref.current.name
    const className = ref.current.className
    await fetching(editSelfProfile, {
      "id": globalState.token, 
      [className]: ref.current.files[0]
    }, {
      "callback": (response) => {
        dispatch({ "type": "EDITPROFILE", "profile": { [name]: ref.current.files[0] } })
        window.location.reload()
      }, 
      //"log": "ProfileHeader.handleUploadPicture"
    })
  }

  return (
    <div className="profile-header">
      <img className="profilecover" src={displayCover} alt="cover" />
      {isMe ? 
        <>
          <input ref={coverRef} type="file" accept="image/*" name="cover" className="cover_picture" onChange={(e) => handleUploadPicture(e, coverRef)} />
          <A className="coveredit" onClick={() => coverRef.current.click()} href="#">
        <Camera color="#cccccc" />
      </A>
        </>
        : <></>
      }
      <div className="avatar">
        <img src={displayAvatar} alt="avatar" />
        {isMe ? 
          <>
            <input ref={avatarRef} type="file" accept="image/*" name="avatar" className="profile_picture" onChange={(e) => handleUploadPicture(e, avatarRef)} />
            <A onClick={() => avatarRef.current.click()} href="#">Edit</A>
          </>
          : <></>
        }
      </div>
      <h1 className="fullname">{firstname} {lastname}</h1>
      <div className="fullnamebackground"></div>
      {isMe ?
        <></> :
        <FriendController data={{ useFriendStatus, username }} />
      }
      <div className="menu">
        <div>
          <A onClick={() => setView('timeline')} style={view === 'timeline' ? activeStyle : {}} href={`/${username}`}>
            Timeline
          </A>
          <div className="arrowup9" style={checkView(view === 'timeline')} />
        </div>
        <div>
          <A onClick={() => setView('about')} style={view === 'about' ? activeStyle : {}} href={`/${username}/about`}>
            About
          </A>
          <div className="arrowup9" style={checkView(view === 'about')} />
        </div>
        {showFriendMenu === 'none' ? 
          <></> : 
          <div>
            <A onClick={() => setView('friends')} style={view === 'friends' ? activeStyle : {}} href={`/${username}/friends`}>
              Friends
            </A>
            <div className="arrowup9" style={checkView(view === 'friends')} />
          </div>
        }
      </div>
    </div>
  )
}

function ProfileItem(props) {
  const { globalState } = useContext(Store)
  const useEditing = useState({
    "now": false, 
    "prev": false
  })
  const [editing, setEditing] = useEditing
  const { head, label, name, privacy, fetchProfilePackage } = props.data
  const { useProfile } = fetchProfilePackage
  const [profile, ] = useProfile
  const [value, setValue] = useState()
  const inputRef = useRef()

  const cancelEdit = (e) => {
    setEditing({ ...editing, "now": false })
    inputRef.current.innerText = value
  }

  const handleUpload = async (e) => {
    if (e.keyCode === 27) cancelEdit()
    if (e.keyCode === 13) {
      if (e.shiftKey) return
      setEditing({ ...editing, "now": false })
      let originText = e.target.innerText
      let newText = originText
      if (originText.substring(originText.length - 1, originText.length) === '\n') {
        newText = originText.substring(0, originText.length - 1)
      }
      if (originText.length > 255) {
        newText = newText.substring(0, 255)
      }
      await fetching(editSelfProfile, {
        "id": globalState.token, 
        [name]: newText
      }, {
        "callback": async (response) => {
          await fetchProfile(fetchProfilePackage)
        }, 
        //"log": "ProfileItem.handleUpload"
      })
    }
  }

  const placeCaretAtEnd = (el) => {
    el.focus()
    if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
        let range = document.createRange()
        range.selectNodeContents(el)
        range.collapse(false)
        let sel = window.getSelection()
        sel.removeAllRanges()
        sel.addRange(range)
    } else if (typeof document.body.createTextRange != "undefined") {
        let textRange = document.body.createTextRange()
        textRange.moveToElementText(el)
        textRange.collapse(false)
        textRange.select()
    }
  }

  useEffect(() => {
    if (editing.now && !editing.prev) {
      placeCaretAtEnd(inputRef.current)
    }
    setEditing({ ...editing, "prev": editing.now })
  }, [editing.now])

  useEffect(() => {
    setValue(profile[name])
  }, [profile[name]])

  useEffect(() => {
    inputRef.current.innerText = value
  }, [value])


  return (
    <>
      {head ? 
        <li className="head">
          {/*<i class="globe" aria-hidden="true" tabindex="-1"></i>*/}
          {head}
        </li> 
        : <></>
      }
      <li key={name} className={name}>
        {label ? <span className="label">{label}</span> : <></>}
        <div ref={inputRef} className={label ? 'editablediv withlabel' : 'editablediv'} contentEditable={editing.now} onKeyUp={handleUpload} onBlur={cancelEdit} />
        {globalState.profile.username === profile.username && !editing.now ?
          <PrivacySelector data={{ "useFor": "profileitem", name, privacy }} />
          : <></>
        }
        {props.editable && !editing.now ? 
          <span className="editbutton" onClick={() => {
            setEditing({ ...editing, "now": true})
          }}>Edit</span>
          : <></>
        }
      </li>
    </>
  )
}

export default Profile