const calculateCommentTime = (now, commenttime) => {
  let commentTime = commenttime
  if (!Number.isInteger(commenttime)) {
    if (Number.isInteger(parseInt(commenttime))) {
      commentTime = parseInt(commenttime)
    } else {
      return commenttime
    }
  }
  let diffTime = now - commentTime
  if (diffTime < 60000) {
    return 'just now'
  }
  if (diffTime < 120000) {
    return '1 minute ago'
  }
  if (diffTime < 3600000) {
    return `${Math.floor(diffTime/60000)} minutes ago`
  }
  if (diffTime < 7200000) {
    return `1 hour ago`
  }
  if (diffTime < 86400000) {
    return `${Math.floor(diffTime/3600000)} hours ago`
  }
  if (diffTime < 172800000) {
    let yesterday = new Date(now - 86400000)
    let commentDate = new Date(commentTime)
    if (yesterday.getDate() === commentDate.getDate()) {
      return `yesterday`
    } else {
      return `2 days ago`
    }
  }
  if (diffTime < 604800000) {
    let oneWeekTimeDate = new Date(commentTime + 604800000)
    let nowDate = new Date(now)
    if (oneWeekTimeDate.getDate() === nowDate.getDate()) {
      return `1 week ago`
    } else {
      return `${Math.floor(diffTime/86400000)} days ago`
    }
  }
  if (diffTime < 1209600000) {
    return `1 week ago`
  }
  return `${Math.floor(diffTime/604800000)} weeks ago`
}

const fetching = async (path, formdataobj, options) => {
  const formdataObj = formdataobj || {}
  const { callback, log, signal } = options || { "callback": () => {}, "log": '', "signal": undefined }
  const formData = new FormData()
  for (let each of Object.entries(formdataObj)) {
    formData.append(each[0], each[1])
  }
  try {
    let res
    if (formdataobj) {
      res = await fetch(path, {
        "method": "post",
        "credentials": 'include', 
        "body": formData, 
        signal
      })
    } else {
      res = await fetch(path, { 
        "method": "get",
        signal 
      })
    }
    const response = await res.json()
    if (log) {
      console.log(`${log}: ${res.status} ${response.status}`)
      console.log(response)
    }
    callback(response)
  } catch (err) {
    if (log) console.log(`${log}: (err) ${err}`)
  }
}

const getMonthString = (monthNumber) => {
  switch (monthNumber) {
    case 0:
      return 'January'
    case 1:
      return 'February'
    case 2:
      return 'March'
    case 3:
      return 'April'
    case 4:
      return 'May'
    case 5:
      return 'June'
    case 6:
      return 'July'
    case 7:
      return 'August'
    case 8:
      return 'September'
    case 9:
      return 'October'
    case 10:
      return 'November'
    case 11:
      return 'December'
    default:
      return ''
  }
}

export { calculateCommentTime, fetching, getMonthString }